package org.example.battleship.pl.controller;

import org.example.battleship.pl.entity.Player;
import org.example.battleship.pl.game.Point;
import org.example.battleship.pl.service.PlayerService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "deleteShip", value = "/deleteShip")
public class DeleteShipController   extends HttpServlet {
    PlayerService playerService = new PlayerService();




    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String coordinates = req.getParameter("position");
       Point point = new Point();
       point.decode(coordinates);
    Player player = (Player)req.getSession().getAttribute("player");
        Integer numberOfShip = player.numberOfShipOn(point);
        player.deleteShipOnNumber(numberOfShip);
    playerService.save(player);
    req.getSession().setAttribute("player", player);
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/join");
        dispatcher.forward(req, resp);

    }
}
