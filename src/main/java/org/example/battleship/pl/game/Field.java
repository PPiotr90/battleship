package org.example.battleship.pl.game;

public enum Field {
    SHIP,EMPTY, MISHIT, HUNTED
}
