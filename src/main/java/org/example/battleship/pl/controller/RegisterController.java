package org.example.battleship.pl.controller;

import org.example.battleship.pl.dto.RegisterDTO;
import org.example.battleship.pl.entity.User;
import org.example.battleship.pl.service.RegisterService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "create", value = "/create")
public class RegisterController extends HttpServlet {

    private RegisterService registerService = new RegisterService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/create.jsp");
        requestDispatcher.forward(req, resp);
    }

        @Override
        protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            String login = req.getParameter("login");
            String password = req.getParameter("password");
            String repeatedPassword = req.getParameter("RepeatedPassword");
            if (password.equals(repeatedPassword)) {
                User userByLogin = registerService.findUserByLogin(login);

                if (userByLogin != null) {
                    RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/create");
                    dispatcher.forward(req, resp);
                } else {
                    RegisterDTO registerDTO = new RegisterDTO(login, password, repeatedPassword);
                    registerService.save(registerDTO);
                }
                User userFromDataBase = registerService.findUserByLogin(login);
                if (password.equals(userFromDataBase.getPassword())) {
                    RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/login");
                    dispatcher.forward(req, resp);
                }
            }
        }
    }
