package org.example.battleship.pl.controller;

import org.example.battleship.pl.entity.Game;
import org.example.battleship.pl.entity.Player;
import org.example.battleship.pl.service.PlayerService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "game", value = "/play")
public class PlayController  extends HttpServlet {
    PlayerService playerService = new PlayerService();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Game game = (Game) req.getSession().getAttribute("game");
        Player player = (Player) req.getSession().getAttribute("player");
        player.setReady(true);
        playerService.save(player);
        game.getReady();
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/play.jsp");
        dispatcher.forward(req, resp);


    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req, resp);
    }
}
