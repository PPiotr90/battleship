package org.example.battleship.pl.repository;

import org.example.battleship.pl.entity.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import org.hibernate.query.Query;

import javax.persistence.NoResultException;
import java.util.Optional;

public class UserRepository {

    SessionFactory sessionFactory = RepositoryUtilities.getSessionFactory();

    public User save(User user) {

        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.saveOrUpdate(user);
        } catch (Exception e) {
            e.printStackTrace();
            transaction.rollback();
        }
        transaction.commit();
        session.close();

        return user;
    }

    public Optional<User> findUserByLogin(String login) {
        Session session = sessionFactory.openSession();
        Query<User> userQuery = session.createQuery("FROM  User where login =:param", User.class);
        userQuery.setParameter("param", login);
        User userFormDB = null;
        Optional<User> result = Optional.ofNullable(userFormDB);
        try{
            userFormDB= userQuery.getSingleResult();
        }
        catch (NoResultException nre) {}
        finally {
            result = Optional.ofNullable(userFormDB);
        }

        return result;
    }
}

