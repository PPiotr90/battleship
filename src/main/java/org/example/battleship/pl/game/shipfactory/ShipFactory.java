package org.example.battleship.pl.game.shipfactory;

import org.example.battleship.pl.game.Point;
import org.example.battleship.pl.game.Ship;

public interface ShipFactory {

    public Ship buildShip(int size, Point start);
}
