<%@ page import="org.example.battleship.pl.entity.Player" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page isELIgnored="false" %>
<%--
  Created by IntelliJ IDEA.
  User: ppali
  Date: 16.07.2020
  Time: 17:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Set fleet</title>
    <link rel="stylesheet" href="css/style.css" style="text/css"/>
</head>
<h1>Here you can set your fleet</h1>
<br>
<br>
<form id="ship" action="addShip" method="POST">
    1. Select ship and direction<br>
    <c:forEach var="i" items="${player.availableSize}">
        <c:out value="${i}"/><input type="radio" value="${i}" name="size"><br>
    </c:forEach>

    <select name="direction">
        <option>Horizontal</option>
        <option>Vertical</option>
    </select>


<br>
2. Select start position<br>
Ship will be build to down or to right
<table>
    <thead>
    <tr>
        <th></th>
        <c:forEach var="x" begin="0" end="9" step="1" >

            <th><c:out value="${x}"/></th>
        </c:forEach>

    </tr>
    </thead>
    <tbody>
    <c:set var="first" value="1"></c:set>
    <c:forEach var="y" begin="0" end="9" step="1">
        <tr><td><c:out value="${y}"/></td>
        <c:forEach var="x" begin="0" end="9" step="1">
            <td>
                <c:set var="result" value="${player.isPartOfShip(x,y)}"></c:set>
                <c:if test="${result==0}">
                    <input type="radio" name="coordinates", value="${x},${y}"/>
                    <c:set var="first" value="1"></c:set>
                </c:if>

                <c:if test="${result==1}">

                    <div class="ship">
                        <c:if test="${first==1}">

                            <h6><a class="deleteLink" href="deleteShip?position=${x},${y}" />X</a>
                            <c:set var="first" value="0"></c:set></h6>
                        </c:if>
                    </div>
            </c:if>




            </td>
        </c:forEach>
        </tr>
    </c:forEach>
    
    </tbody>
</table>
    By red X link you can delete boat<br>
    <input type="submit" value="Add">
</form>
<br>

<c:if test="${player.availableSize.isEmpty()}">
    <form action="play" method="POST" >
        <input type="submit" value="PLAY">
    </form>
</c:if>

</body>
</html>


