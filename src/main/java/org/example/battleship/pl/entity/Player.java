package org.example.battleship.pl.entity;

import lombok.Data;

import lombok.NoArgsConstructor;
import org.example.battleship.pl.game.*;
import org.example.battleship.pl.game.shipfactory.ShipFactory;
import org.example.battleship.pl.game.shipfactory.ShipFactoryProducer;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;

@Data
@Entity
@Table
@NoArgsConstructor
public class Player {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn
    private User user;
    @Column(length = 2000)
    @Lob
    private String enemyBoardInString = new String();
    @Column
    private String shipsInString = new String();
    @Column
    private int xBoundary;
    @Column
    private int yBoundary;
    @Transient
    private Ship[] ships = new Ship[8];
    @Transient
    private ArrayList<ResultOfShot> ownBoard = new ArrayList<>();
    @Transient
    private ArrayList<ResultOfShot> enemyBoard = new ArrayList<>();
    @Transient
    private final String SHOT_RESULT_SEPARATOR = ";";
    @Transient
    private final String SHIP_SEPARATOR = "#";
    @Transient
    private ArrayList<Integer> availableSize = new ArrayList<>();
    @Transient
    private ShotMethod shotMethod;
    @Column
    String list = "1;1;2;2;3;3;4;5";
    @Column
    private boolean ready;

    public Player(long id, User user, String enemyBoardInString, String shipsInString, int xBoundary, int yBoundary) {
        this.xBoundary = xBoundary;
        this.yBoundary = yBoundary;
        this.id = id;
        this.user = user;
        this.enemyBoardInString = enemyBoardInString;
        this.shipsInString = shipsInString;
        ready=false;
    }

    {


    }


    public void shot(Point objective) {
        shotMethod.shot(objective);
    }


    public void addShip(Ship ship) {
        if (PositionOfShipIsCorrect(ship)) {
            for (int i = 0; i < ships.length; i++) {
                if (ships[i] == null) {
                    ships[i] = ship;

                    int size = ship.getMasts().length;
                    availableSize.remove((Integer) size);
                    this.encodeShipsToString();
                    encodeAvailableList();
                    this.decodeAvailableList();
                    break;
                }
            }
        }
    }

    private boolean PositionOfShipIsCorrect(Ship NewShip) {
        {
            for (Point mast : NewShip.getMasts()) {
                if (mastIsOutOfBounds(mast)
                        || mastIsNextToOtherShip(mast)) {
                    return false;
                }
            }
            return true;
        }
    }

    private boolean mastIsOutOfBounds(Point mast) {

        return mast.getX() > xBoundary - 1
                || mast.getY() > yBoundary - 1;
    }


    private boolean mastIsNextToOtherShip(Point maNewMastt) {
        for (Ship ship : ships) {
            if (ship != null && PointISNetToShip(maNewMastt, ship)) {
                return true;

            }
        }
        return false;
    }

    private boolean PointISNetToShip(Point newMast, Ship ship) {
        for (Point mastOfShip : ship.getMasts()) {
            if (PointsAreNeighbors(newMast, mastOfShip)) {
                return true;
            }
        }
        return false;
    }

    private boolean PointsAreNeighbors(Point newMast, Point mastOfShip) {

        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                Point neighbor = new Point(newMast.getX() + i, newMast.getY() + j);
                if (neighbor.equals(mastOfShip)) {
                    return true;
                }

            }
        }
        return false;
    }


    public AfterShot underAttack(Point point) {

        for (Ship ship : ships) {
            if (ship != null
                    && ship.isStruck(point)) {
                if (ship.isSunk()) {
                    return AfterShot.HUNTED_AND_SUNK;
                } else return AfterShot.HUNTED;

            }

        }
        return AfterShot.MISHIT;
    }

    public void setNeighborsHuntedAsHuntedAndSunk(Point point, String owner) {
        for (int i=-1; i<2; i+=2) {
            for (int j = -1; j < 2; j+=2) {
                Point neighbor = new Point(point.getX()+i,point.getY()+j);
                ArrayList<ResultOfShot> board = getBoard(owner);
                for(ResultOfShot resultOfShot:board) {
                    {
                        if (resultOfShot.getAfterShot()!=null
                                &&resultOfShot.getPoint() !=null
                        && resultOfShot.getPoint().equals(neighbor) &&
                                resultOfShot.getAfterShot().equals(AfterShot.HUNTED)) {
                            resultOfShot.setAfterShot(AfterShot.HUNTED_AND_SUNK);
                        }
                    }
                }
            }
        }
    }

    public void encodeEnemyBoardToString() {
        String result = new String();
        for (ResultOfShot resultOfShot : enemyBoard) {
          if(resultOfShot.getPoint()!=null
          && resultOfShot.getAfterShot()!=null)  {
                result += resultOfShot.encode();
                result += SHOT_RESULT_SEPARATOR;
                this.setEnemyBoardInString(result);
            }
        }


    }

    public void decodeEnemyBoardFromString() {
        if (enemyBoardInString != null) {
            if (enemyBoardInString.endsWith(SHOT_RESULT_SEPARATOR))
                enemyBoardInString = enemyBoardInString.substring(0, enemyBoardInString.length() - 1);
            String[] resultsInStrings = enemyBoardInString.split(SHOT_RESULT_SEPARATOR);
            for (String resultInString : resultsInStrings) {
                ResultOfShot result = new ResultOfShot();
                result.decode(resultInString);
                enemyBoard.add(result);
            }
        }
        else
            enemyBoard = new ArrayList<>();
    }

    public void encodeShipsToString() {
        String result = new String();
        for (Ship ship : ships) {
            if (ship != null) {
                result += ship.encode();
                result += SHIP_SEPARATOR;

            }
        }
        if (result.length()>0)
                result = result.substring(0,result.length()-1);
                this.setShipsInString(result);
    }

    public void decodeShipsFromString() {
        if (shipsInString != null && !shipsInString.equals("")) {
            if (shipsInString.endsWith(SHIP_SEPARATOR))
                shipsInString = shipsInString.substring(0, shipsInString.length() - 1);
            String[] shipsInStrings = shipsInString.split(SHIP_SEPARATOR);
            int i = 0;
            for (String shipInString : shipsInStrings) {
                Ship shipFromString = new Ship();
                shipFromString.decode(shipInString);
                ships[i] = shipFromString;
                i++;
            }
        }
    }

    public int isPartOfShip(int x, int y) {
        Point point = new Point(x, y);
        for (Ship ship : ships) {
            if (ship != null) {
                for (Point mast : ship.getMasts()) {
                    if (mast != null && mast.equals(point)) {
                        return 1;
                    }
                }
            }
        }
        return 0;
    }

    private void encodeAvailableList() {
        String result = new String();
        for (int i : availableSize) {
            result += i;
            result += ";";
        }
        if (availableSize.size() > 0)
            result = result.substring(0, result.length() - 1);
        list = result;
    }

    private void decodeAvailableList() {

        String[] split = list.split(";");
        availableSize = new ArrayList<>();
        for (String s : split) {
            if (!s.equals(""))
                availableSize.add(Integer.parseInt(s));
        }
    }

    public void encode() {
        this.encodeShipsToString();
        this.encodeEnemyBoardToString();
        this.decodeAvailableList();

    }

    public int partOfOwnBoard(int x, int y) {
        int result = partOfBoard(x, y, "my");
        if(result==0) {
            result = this.isPartOfShip(x, y)*4;
        }
        return  result;

    }

    public int partOfEnemyBoard(int x, int y) {
        return partOfBoard(x, y, "enemy");
    }

    private int partOfBoard(int x, int y, String owner) {
        Point objective = new Point(x, y);
        ArrayList<ResultOfShot> board = this.getBoard(owner);
        AfterShot afterShot= null;
        for (ResultOfShot resultOfShot : board) {
            Point pointOfShot = resultOfShot.getPoint();
            if (pointOfShot!=null && pointOfShot.equals(objective)){
                afterShot = resultOfShot.getAfterShot();
        }
        if(afterShot==null) return 0;



    }
        return this.codeAfterShot(afterShot);
    }
    private  int codeAfterShot(AfterShot afterShot) {
        switch (afterShot) {
            case HUNTED_AND_SUNK:
                return 3;
            case MISHIT:
                return 1;
            case HUNTED:
                return 2;
            default:
                return 0;
        }
    }


    private ArrayList<ResultOfShot> getBoard(String owner) {
        ArrayList<ResultOfShot> board;
        switch (owner) {
            case "enemy":
                board = enemyBoard;
                break;
            default:
                board = ownBoard;
        }
        return board;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return id == player.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public  boolean wasShotInPast(Point objective) {
        for(ResultOfShot resultOfShot: enemyBoard) {
            if(resultOfShot.getPoint()!=null
            && resultOfShot.getPoint().equals(objective))
                return  true;
        }
        return  false;
    }

    public  Integer numberOfShipOn(Point position) {
        for(int i=0; i<ships.length; i++) {
            if(ships[i]!=null
            && ships[i].isPartOfShip(position))
                return  i;
        }

            return null;
        }

    public void deleteShipOnNumber(int numberOfShip) {
        Integer size = ships[numberOfShip].getMasts().length;
        ships[numberOfShip]=null;
        this.decodeAvailableList();
        availableSize.add(size);
        Collections.sort(availableSize);
        this.encodeAvailableList();
    }
}
