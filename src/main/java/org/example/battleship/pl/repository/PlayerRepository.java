package org.example.battleship.pl.repository;

import org.example.battleship.pl.entity.Player;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;


public class PlayerRepository {
    SessionFactory sessionFactory = RepositoryUtilities.getSessionFactory();

    public Player save(Player player) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        try {
            session.saveOrUpdate(player);
        }
        catch (Exception e) {
            transaction.rollback();
        }
        transaction.commit();
            session.close();


        return player;
    }
}
