package org.example.battleship.pl.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.example.battleship.pl.dto.RegisterDTO;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@EqualsAndHashCode
@Table(name = "users")
public class User {
@Id
@GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
@Column
    private String login;
@Column
    private  String password;


    public User(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public  User(RegisterDTO registerDTO) {
        this.login = registerDTO.getLogin();
        this.password= registerDTO.getPassword();
    }
}
