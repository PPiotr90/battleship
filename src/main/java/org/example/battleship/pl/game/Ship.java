package org.example.battleship.pl.game;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.PipedOutputStream;


@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class Ship implements Encodable {

    private final String MAST_SEPARATOR = "<";
    private Point[] masts;


    public boolean isStruck(Point point) {
        for (int i = 0; i < masts.length; i++) {
            if (masts[i] != null
                    && masts[i].equals(point)) {
                destroyMast(i);
                return true;
            }
        }

        return false;
    }

    private void destroyMast(int n) {
        masts[n] = null;
    }

    public boolean isSunk() {
        for (Point mast : masts) {
            if (mast != null) return false;
        }
        return true;
    }

    @Override
    public String encode() {
        String result = new String();
        for (Point mast : masts) {
            if (mast == null) {
                result += "null";
            } else result += mast.encode();
            result += MAST_SEPARATOR;

        }
        result = result.substring(0,result.length()-1);
        return result;
    }

    @Override
    public void decode(String code) {
        String[] mastsInString = code.split(MAST_SEPARATOR);
        Point[] decodedMasts = new Point[mastsInString.length];
        for (int i = 0; i < mastsInString.length; i++) {
            String mastInString = mastsInString[i];

            if (mastInString.equals("null")) {
                decodedMasts[i] = null;
            } else {
                decodedMasts[i] = new Point();
                decodedMasts[i].decode(mastInString);
            }

        }
        this.setMasts(decodedMasts);

    }

    public boolean isPartOfShip(Point point) {
for(Point mast:masts) {
    if(point.equals(mast))
        return  true;
}
return  false;
    }
}

