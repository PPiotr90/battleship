package org.example.battleship.pl.service;

import org.example.battleship.pl.entity.Game;
import org.example.battleship.pl.entity.Player;
import org.example.battleship.pl.repository.GameRepository;
import org.example.battleship.pl.repository.PlayerRepository;

import java.util.List;

public class GameService {
    GameRepository gameRepository = new GameRepository();
    PlayerRepository playerRepository = new PlayerRepository();


    public List<Game> findAll() {
        return gameRepository.findAll();
    }


    public Game create(Game game) {
        Player player1 = new Player();
        Player player2 = new Player();
        player1=playerRepository.save(player1);
        player2=playerRepository.save(player2);
        game.setPlayer1(player1);
        game.setPlayer2(player2);


        return gameRepository.save(game);

    }
    public Game update(Game game) {



        return gameRepository.save(game);

    }
    public  Game findGameById(Long id) {
        return gameRepository.findGameById(id).get();
    }
}
