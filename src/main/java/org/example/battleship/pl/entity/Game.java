package org.example.battleship.pl.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.example.battleship.pl.game.AfterShot;
import org.example.battleship.pl.game.ResultOfShot;

import javax.persistence.*;

@Data
@Entity
@Table
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Game {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column
    private  int xBoundary;
    @Column
    private  int yBoundary;
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn
    private Player player1 = null;
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn
    private Player player2 = null;
    @Column
    private int turnOfPlayer;
    @Column
    private boolean open=true;
    @Transient
    int show;


    public Game(int xBoundary, int yBoundary) {
        player1 = new Player();
        player2= new Player();
        open = true;
        turnOfPlayer = 1;
        this.xBoundary = xBoundary;
        this.yBoundary = yBoundary;
    }


    private void setShotFunctions() {
        this.setShotMethod(player1);
        this.setShotMethod(player2);
    }
    private  void setShotMethod(Player player) {
        Player anotherPlayer = anotherPlayer(player);
        player.setShotMethod(objective -> {if(!player.wasShotInPast(objective)){
                AfterShot afterShot = anotherPlayer.underAttack(objective);
                ResultOfShot resultOfShot = new ResultOfShot(objective, afterShot);
            player.getEnemyBoard().add(resultOfShot);

                player.encodeEnemyBoardToString();

            if (afterShot.equals(AfterShot.HUNTED_AND_SUNK))
                player.setNeighborsHuntedAsHuntedAndSunk(objective, "enemy");
                anotherPlayer.setOwnBoard(player.getEnemyBoard());

                if (afterShot.equals(AfterShot.MISHIT))
                    this.nextPlayer();

            }
        });
    }
    public Player anotherPlayer(Player player) {
        if(player.equals(player1))
            return  player2;
        else  return player1;
    }

    private void nextPlayer() {
        turnOfPlayer++;
        if (turnOfPlayer > 2) {
            turnOfPlayer = 1;
        }
    }

    public Player connect(User newUser) {
        if (player1.getUser()!=null &&
                player1.getUser().equals(newUser))
            return player1;
        else if (player2.getUser()!=null &&
                player2.getUser().equals(newUser))
            return player2;
        else if (open) {
            if (player1.getUser() == null) {
                player1.setUser(newUser);
                return player1;

            } else {
                player2.setUser(newUser);
                open = false;
                return player2;
            }
        }
        return  null;
    }

    public  void getReady(){
        this.setShotFunctions();
        player1.decodeEnemyBoardFromString();
        player1.decodeShipsFromString();
        player2.decodeEnemyBoardFromString();
        player2.decodeShipsFromString();
        player1.setOwnBoard(player2.getEnemyBoard());
        player2.setOwnBoard(player1.getEnemyBoard());
    }

    public void setShow(User user) {
        if(open || user.equals(player1.getUser()) || user.equals(player2.getUser()))
            show=1;
        else
            show =0;
    }

    public  Player actualPlayer() {
        if(turnOfPlayer==1)
            return player1;
        else
            return player2;


    }



}
