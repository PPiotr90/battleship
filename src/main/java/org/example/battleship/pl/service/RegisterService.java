package org.example.battleship.pl.service;

import org.example.battleship.pl.repository.UserRepository;
import org.example.battleship.pl.dto.RegisterDTO;
import org.example.battleship.pl.entity.User;

import java.util.Optional;

public class RegisterService {

    UserRepository userRepository = new UserRepository();

    public void save(RegisterDTO registerDTO) { ;
        if (!(registerDTO.getPassword().equals(registerDTO.getRepeatedPassword()))) {

         throw new RuntimeException("password are not equal");
        }
else if(registerDTO.getLogin().equals(userRepository.findUserByLogin(registerDTO.getLogin()).isPresent())) {
    throw new  RuntimeException("in db exist user with this login");
        }
else {
    User userToSave = new User(registerDTO);
    userRepository.save(userToSave);
        }
    }

    public  User findUserByLogin(String login) {

        Optional<User> optionalUser = userRepository.findUserByLogin(login);

        if(optionalUser.isEmpty()) {
            return  null;
        }
        else return optionalUser.get();

    }

}
