package org.example.battleship.pl.entity;

import io.codearte.jfairy.Fairy;
import org.example.battleship.pl.game.Direction;
import org.example.battleship.pl.game.Point;
import org.example.battleship.pl.game.Ship;
import org.example.battleship.pl.game.shipfactory.ShipFactory;
import org.example.battleship.pl.game.shipfactory.ShipFactoryProducer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Locale;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class PlayerTest {
    Random random;
    Player player;
    @BeforeEach
    public  void  start() {
       random  = new Random();
       player = new Player();

    }

    @Test
    public  void  decodingAndEncodingShipsTest() {
        Ship[] ships = new  Ship[10];
        for (int i = 0; i < ships.length; i++) {
            int x = (int)(random.nextDouble()*9);
            int y = (int)(random.nextDouble()*9);
            int size = (int)(random.nextDouble()*9);
            Point start = new Point(x, y);
            ShipFactory shipFactory = ShipFactoryProducer.createShipFactory(Direction.HORIZONTAL);
            Ship ship = shipFactory.buildShip(size, start);
            ships[i]=ship;
        }
            player.setShips(ships);
            player.encodeShipsToString();
            player.decodeShipsFromString();
            Ship[] shipsFromUser = player.getShips();

                org.assertj.core.api.Assertions.assertThat(ships).isEqualTo(shipsFromUser);




        }

    }
