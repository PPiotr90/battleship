package org.example.battleship.pl.game;

import org.assertj.core.api.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PointTest {
    Point test = new Point(0,0);
    @BeforeEach
    public  void  startTest() {
        test = new Point(17,15);
    }

    @Test
    public void  codingShouldGetResult() {
        String codedPoint = test.encode();
        String expectedResult ="17,15";
        assertEquals(expectedResult, codedPoint);

    }

}