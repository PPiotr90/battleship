package org.example.battleship.pl.game;

public enum Direction {

 VERTICAL, HORIZONTAL;
 public  static Direction findByString(String s) {
  if(s.equals("Horizontal")) return Direction.HORIZONTAL;
  if(s.equals("Vertical")) return Direction.VERTICAL;
  else  return  null;
 }
}
