package org.example.battleship.pl.game;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Point  implements  Encodable{

    private  int x;
    private  int y;
    private  final String POINT_VALUES_SEPARATOR =",";


    @Override
    public String encode() {
        String result = String.valueOf(x);
        result+= POINT_VALUES_SEPARATOR;
        result+=String.valueOf(y);
        return  result;
    }

    @Override
    public void decode(String code) {
        if(!code.equals("")){
            String[] split = code.split(POINT_VALUES_SEPARATOR);
            String[] values = split;
            int xFromCode = Integer.parseInt(values[0]);
            int yFromCode = Integer.parseInt(values[1]);
            this.setX(xFromCode);
            this.setY(yFromCode);
        }

    }
}
