<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page isELIgnored="false" %>
<html>
<head>
    <title> Logged page</title>

</head>
<body>
<h1> You are logged as
    <c:out value="${loggedUser.login}"/>
</h1>

<form action="createGame" method="POST">
    <input type="submit" value="create new game">
</form>

<c:forEach var="game" items="${allGames}">
    game id: <c:out value="${game.id}"/>
    <br>
    Player 1: <c:out value="${game.player1.user.login}"/>
    <br>
    Player 2: <c:out value="${game.player2.user.login}"/>

<br>

    <c:if test="${game.show==1}">
        <form action="join" method="POST">
            <input type="hidden" name="gameId" value="${game.id}">
            <input type="submit" , value="Join" />
        </form>
    </c:if>
    <br>
</c:forEach>
</body>
</html>