package org.example.battleship.pl.game.shipfactory;

import org.example.battleship.pl.game.Direction;

public class ShipFactoryProducer {
    public static ShipFactory createShipFactory(Direction direction) {
        switch (direction) {
            case VERTICAL:
                return new VerticalShipFactory();
            case HORIZONTAL:
                return new HorizontalShipFactory();
            default:
                throw new RuntimeException("Unknown direction");
        }

    }
}
