package org.example.battleship.pl.controller;

import org.example.battleship.pl.entity.Game;
import org.example.battleship.pl.entity.User;
import org.example.battleship.pl.service.GameService;
import org.example.battleship.pl.service.RegisterService;

import javax.management.Attribute;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "login", value = "/login")
public class LoginController extends HttpServlet {

    private RegisterService registerService = new RegisterService();
    private GameService gameService = new GameService();


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/login.jsp");
        requestDispatcher.forward(req, resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User loggedUser = (User) req.getSession().getAttribute("loggedUser");
        String login = req.getParameter("login");
        String password = req.getParameter("password");

        if (loggedUser==null)
            loggedUser = registerService.findUserByLogin(login);
        if(password!=null
        && !loggedUser.getPassword().equals(password))
                throw new RuntimeException("password is wrong");

        List<Game> games = gameService.findAll();
        User finalLoggedUser = loggedUser;
        games.forEach(game -> game.setShow(finalLoggedUser));
        req.getSession().setAttribute("loggedUser", loggedUser);
        req.getSession().setAttribute("allGames", games);
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/logged.jsp");
        dispatcher.forward(req, resp);


    }
}
