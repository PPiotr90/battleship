
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page isELIgnored="false" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>

</head>
<h1>Here you can play in your game</h1>
<link rel="stylesheet" href="css/style.css" style="text/css"/>

<br>
Player1 ready:<c:out value="${game.player1.ready}"/><br>
Player2 ready:<c:out value="${game.player2.ready}"/><br>
<br>
<br>

    This is board of your enemy:<br>
    You can attack by buttons.<br><br>
    <table>
        <thead>
        <tr>
            <th></th>
            <c:forEach var="x" begin="0" end="9" step="1" >
                <th><c:out value="${x}"/></th>
            </c:forEach>

        </tr>
        </thead>
        <tbody>
        <c:forEach var="y" begin="0" end="9" step="1">
            <tr><td><c:out value="${y}"/></td>
                <c:forEach var="x" begin="0" end="9" step="1">
                    <td>
                        <c:set var="result" value="${player.partOfEnemyBoard(x,y)}"></c:set>
                        <c:if test="${result==3}">
                            <div class="sunk"></div>
                        </c:if>
                        <c:if test="${result==2}">
                            <div class="hunted"></div>
                        </c:if>
                        <c:if test="${result==1}">
                            <div class="mishit"></div>
                        </c:if>
                        <c:if test="${result==0}">
                            <form method="POST" action="attack">
                                <input type="hidden" name="objective", value="${x},${y}">
                                <input type="submit" value=" " class="button">
                            </form>
                        </c:if>

                    </td>
                </c:forEach>
            </tr>
        </c:forEach>

        </tbody>
    </table>
    <br>
    <c:if test="${!game.open}">
    <c:out value="Turn of user:"/>
    <br>
    <c:if test="${game.turnOfPlayer==1}">
    <c:out value="${game.player1.user.login}"/>
    </c:if>
    <c:if test="${game.turnOfPlayer==2}">
        <c:out value="${game.player2.user.login}"/>
    </c:if>
    </c:if>
    <br>
    There is your own Board
    <table>
        <thead>
        <tr>
            <th></th>
            <c:forEach var="x" begin="0" end="9" step="1" >

                <th><c:out value="${x}"/></th>
            </c:forEach>

        </tr>
        </thead>
        <tbody>
        <c:forEach var="y" begin="0" end="9" step="1">
            <tr><td><c:out value="${y}"/></td>
                <c:forEach var="x" begin="0" end="9" step="1">
                    <td>
                        <c:set var="result" value="${player.partOfOwnBoard(x,y)}"></c:set>
                        <c:if test="${result==3}">
                        <div class="sunk"></div>
                    </c:if>
                        <c:if test="${result==2}">
                            <div class="hunted"></div>
                        </c:if>
                        <c:if test="${result==1}">
                            <div class="mishit"></div>
                        </c:if>
                        <c:if test="${result==4}">
                            <div class="ship"></div>
                        </c:if>

                    </td>
                </c:forEach>
            </tr>
        </c:forEach>

        </tbody>
    </table>
    <br>
    Legend:<br>
<div class="ship"></div>-Boat<br>
<div class="mishit"></div>-Mishit<br>
<div class="hunted"></div>-Hunted<br>
<div class="sunk"></div>-Sunk<br>

</body>
</html>


