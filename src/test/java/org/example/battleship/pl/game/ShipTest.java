package org.example.battleship.pl.game;

import io.codearte.jfairy.Fairy;
import org.example.battleship.pl.game.shipfactory.ShipFactory;
import org.example.battleship.pl.game.shipfactory.ShipFactoryProducer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ShipTest {

    ShipFactory verticalShipFactory;
    ShipFactory horizontalShipFactory;

    @BeforeEach
    public  void start() {
        verticalShipFactory = ShipFactoryProducer.createShipFactory(Direction.VERTICAL);
        horizontalShipFactory = ShipFactoryProducer.createShipFactory(Direction.HORIZONTAL);
    }

    @Test
    public void EncodeAndDecodeVerticalShipTest() {

        Point start = new Point(7,2);
        int size =4;
        Ship ship = verticalShipFactory.buildShip(size, start);
        String encoding = ship.encode();
        Ship decodingShip = new Ship();
        decodingShip.decode(encoding);

        org.assertj.core.api.Assertions.assertThat(decodingShip).isEqualTo(ship);
    }

    @Test
    public void EncodeAndDecodeHorizontalShipTest() {

        Point start = new Point(7,2);
        int size =4;
        Ship ship = horizontalShipFactory.buildShip(size, start);
        String encoding = ship.encode();
        Ship decodingShip = new Ship();
        decodingShip.decode(encoding);

        org.assertj.core.api.Assertions.assertThat(decodingShip).isEqualTo(ship);
    }

}