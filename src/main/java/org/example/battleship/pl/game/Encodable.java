package org.example.battleship.pl.game;

public interface Encodable {
    public String encode();
    public void decode(String code);
}
