package org.example.battleship.pl.service;

import org.example.battleship.pl.entity.Player;
import org.example.battleship.pl.repository.PlayerRepository;

public class PlayerService {
    PlayerRepository playerRepository = new PlayerRepository();

    public Player save(Player player) {

        player.encode();
        return playerRepository.save(player);

    }
}
