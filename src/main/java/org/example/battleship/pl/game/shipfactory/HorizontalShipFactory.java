package org.example.battleship.pl.game.shipfactory;

import org.example.battleship.pl.game.Point;
import org.example.battleship.pl.game.Ship;

public class HorizontalShipFactory  implements  ShipFactory{
    @Override
    public Ship buildShip(int size, Point start) {
        Point[] masts = new Point[size];
        for (int i = 0; i < size ; i++) {
            masts[i] = new Point();
            masts[i].setX(start.getX()+i);
            masts[i].setY(start.getY());
        }
        return  new Ship(masts);
    }
}
