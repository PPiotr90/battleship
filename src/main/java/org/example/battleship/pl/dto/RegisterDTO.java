package org.example.battleship.pl.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RegisterDTO {
    private  String login;
    private  String password;
    private  String repeatedPassword;


}
