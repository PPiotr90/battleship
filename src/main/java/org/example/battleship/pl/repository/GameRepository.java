package org.example.battleship.pl.repository;

import org.example.battleship.pl.entity.Game;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import org.hibernate.query.Query;


import javax.persistence.NoResultException;
import java.util.List;
import java.util.Optional;

public class GameRepository {

    SessionFactory sessionFactory = RepositoryUtilities.getSessionFactory();

    public List<Game> findAll() {
        Session session = sessionFactory.openSession();
        List<Game> games = session.createQuery("FROM Game ", Game.class)
                .list();
        session.close();
        return games;

    }

    public Game save(Game game) {
        Session session = sessionFactory.openSession();

        Transaction transaction = session.beginTransaction();
        try {
            session.saveOrUpdate(game);
        } catch (Exception e) {
            e.printStackTrace();
            transaction.rollback();
        }
        transaction.commit();
        session.close();

        return game;
    }

    public Optional<Game> findGameById(Long id) {
        Session session = sessionFactory.openSession();
        Query<Game> gameQuery = session.createQuery("FROM  Game where id =:param", Game.class);
        gameQuery.setParameter("param", id);
        Game gameFromDataBase = null;
        Optional<Game> result = Optional.ofNullable(gameFromDataBase);
        try{
            gameFromDataBase= gameQuery.getSingleResult();
        }
        catch (NoResultException nre) {}
        finally {
            result = Optional.ofNullable(gameFromDataBase);
        }

        return result;
    }
}
