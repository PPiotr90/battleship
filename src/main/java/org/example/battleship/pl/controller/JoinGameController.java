package org.example.battleship.pl.controller;


import org.example.battleship.pl.entity.Game;
import org.example.battleship.pl.entity.Player;
import org.example.battleship.pl.entity.User;
import org.example.battleship.pl.service.GameService;
import org.example.battleship.pl.service.PlayerService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "join", value = "/join")
public class JoinGameController  extends HttpServlet {

 GameService gameService = new GameService();
 PlayerService playerService = new PlayerService();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User loggedUser = (User) req.getSession().getAttribute("loggedUser");

        String id= req.getParameter("gameId");

        if(id==null)
            id = (String) req.getSession().getAttribute("gameId");

        req.getSession().setAttribute("gameId", id);
        Long gameId = Long.parseLong(id);
        Game gameFromDataBase = gameService.findGameById(gameId);
        Player playerOfLoggedUser = gameFromDataBase.connect(loggedUser);
        gameFromDataBase.getReady();
        req.getSession().setAttribute("player", playerOfLoggedUser);
        req.getSession().setAttribute("game", gameFromDataBase);
        playerService.save(playerOfLoggedUser);
        gameService.update(gameFromDataBase);

        RequestDispatcher dispatcher;
        if(playerOfLoggedUser.isReady()) {
            dispatcher = getServletContext().getRequestDispatcher("/play");
        }
        else {
            dispatcher = getServletContext().getRequestDispatcher("/setFleet.jsp");
        }
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req, resp);
    }
}
