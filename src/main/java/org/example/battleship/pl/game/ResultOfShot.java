package org.example.battleship.pl.game;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResultOfShot implements Encodable {
    private Point point;
    private AfterShot afterShot;
    private  final  String RESULT_SEPARATOR ="%";
    private  final  String HUNTED_SYMBOL = "H";
    private  final  String MISHIT_SYMBOL = "M";
    private  final  String HUNTED_AND_SUNK_SYMBOL = "HS";

    @Override
    public String encode() {
        String result = new String();
        if(point!=null)
        result = point.encode();
        if(afterShot!=null){
            result += RESULT_SEPARATOR;
            result += this.codeResult(afterShot);
        }
        return result;

    }

    @Override
    public void decode(String code) {
        if(!code.equals("")){
            if(code.endsWith(RESULT_SEPARATOR))
                code = code.substring(0,code.length()-1);
            String[] resultInString = code.split(RESULT_SEPARATOR);
            Point pointFromString = new Point();
            pointFromString.decode(resultInString[0]);
            AfterShot afterShotFromString = this.decodeResult(resultInString[1]);
            this.setPoint(pointFromString);
            this.setAfterShot(afterShotFromString);
        }


    }


    private String codeResult(AfterShot afterShot) {
        switch (afterShot) {
            case HUNTED:
                return HUNTED_SYMBOL;
            case MISHIT:
                return MISHIT_SYMBOL;
            case HUNTED_AND_SUNK:
                return HUNTED_AND_SUNK_SYMBOL;
            default:
                return "";
        }
    }


    private AfterShot decodeResult(String code) {
        switch (code) {
            case HUNTED_SYMBOL:
                return AfterShot.HUNTED;
            case MISHIT_SYMBOL:
                return AfterShot.MISHIT;
            case HUNTED_AND_SUNK_SYMBOL:
                return AfterShot.HUNTED_AND_SUNK;
            default:
                return  null;

        }
    }
}
