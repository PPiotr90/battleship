package org.example.battleship.pl.game;

public interface ShotMethod {

    public void shot(Point objective);
}
