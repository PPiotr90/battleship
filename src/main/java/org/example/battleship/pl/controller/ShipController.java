package org.example.battleship.pl.controller;

import org.example.battleship.pl.entity.Game;
import org.example.battleship.pl.entity.Player;
import org.example.battleship.pl.entity.User;
import org.example.battleship.pl.game.Direction;
import org.example.battleship.pl.game.Point;
import org.example.battleship.pl.game.Ship;
import org.example.battleship.pl.game.shipfactory.ShipFactory;
import org.example.battleship.pl.game.shipfactory.ShipFactoryProducer;
import org.example.battleship.pl.service.PlayerService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "ship", value = "/addShip")
public class ShipController  extends HttpServlet {
    PlayerService playerService = new PlayerService();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        int size = Integer.parseInt(req.getParameter("size"));
        Direction direction = Direction.findByString(req.getParameter("direction"));
        Point start = new Point();
        start.decode(req.getParameter("coordinates"));
        Player player = (Player)req.getSession().getAttribute("player");
        ShipFactory shipFactory = ShipFactoryProducer.createShipFactory(direction);
        Ship newShip = shipFactory.buildShip(size, start);
        Game game = (Game)req.getSession().getAttribute("game");
        player.setXBoundary(game.getXBoundary());
        player.setYBoundary(game.getYBoundary());
        player.addShip(newShip);
        playerService.save(player);

        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/join");
        dispatcher.forward(req, resp);


    }
}
