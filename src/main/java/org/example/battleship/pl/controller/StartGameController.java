package org.example.battleship.pl.controller;

import org.example.battleship.pl.entity.Game;
import org.example.battleship.pl.service.GameService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "createGame", value = "/createGame")
public class StartGameController extends HttpServlet {
    GameService gameService = new GameService();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Game game = new Game(10,10);
        gameService.create(game);


        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/login");
        dispatcher.forward(req, resp);

    }
}
