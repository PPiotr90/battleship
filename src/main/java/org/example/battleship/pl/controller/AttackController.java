package org.example.battleship.pl.controller;

import net.bytebuddy.agent.builder.AgentBuilder;
import org.example.battleship.pl.entity.Game;
import org.example.battleship.pl.entity.Player;
import org.example.battleship.pl.game.Point;
import org.example.battleship.pl.service.GameService;
import org.example.battleship.pl.service.PlayerService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "attack", value = "/attack")
public class AttackController extends HttpServlet {
    GameService gameService = new GameService();
    PlayerService playerService = new PlayerService();


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String objective = req.getParameter("objective");
        if(objective!=null){
            Player player = (Player) req.getSession().getAttribute("player");
            Game game = (Game) req.getSession().getAttribute("game");
            Player turnOwner = game.actualPlayer();


            if (turnOwner.equals(player)) {
                Player anotherPlayer = game.anotherPlayer(player);
                Point goal = new Point();
                goal.decode(objective);
                player.shot(goal);
                playerService.save(player);
                req.getSession().setAttribute("player", player);
                playerService.save(anotherPlayer);
                gameService.update(game);


            }
        }
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/play");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/play");
        dispatcher.forward(req, resp);
    }
}
